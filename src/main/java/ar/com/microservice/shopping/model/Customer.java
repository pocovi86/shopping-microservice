package ar.com.microservice.shopping.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder //permite crear nuevas instancias de nuestra entidad
public class Customer {
    private Long id;
    private String numberID;
    private String firstName;
    private String lastName;
    private String email;
    private String photoUrl;
    private Region region;
    private String state;
}
