package ar.com.microservice.shopping.service;

import ar.com.microservice.shopping.entity.Invoice;

import java.util.List;

public interface InvoiceService {

    /**
     * Devuelve una lista de Invoice
     * @return {@link List} of {@link Invoice}
     */
    List<Invoice> findInvoiceAll();
    /**
     * Crea un Invoice
     * @param invoice
     * @return {@link Invoice}
     */
    Invoice createInvoice(Invoice invoice);

    /**
     * Actualiza un Invoice
     * @param invoice
     * @return {@link Invoice}
     */
    Invoice updateInvoice(Invoice invoice);

    /**
     * Elimina un Invoice
     * @param invoice
     * @return
     */
    Invoice deleteInvoice(Invoice invoice);

    /**
     * Obtiene un Invoice por un id
     * @param id
     * @return {@link Invoice}
     */
    Invoice getInvoice(Long id);
}
